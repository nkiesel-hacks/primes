primes = { 2 }

local function isPrime(num)
   local limit = math.floor(math.sqrt(num))
   for _, i in ipairs(primes) do
      if i > limit then break end
      if (num % i == 0) then return false end
   end
   primes[#primes + 1] = num
   return true
end

local function answer(n)
   local start = tonumber(n)
   if start == 0 then return "23571" end
   local digits = nil
   local length = 0
   local i = 3
   while true do
      if isPrime(i) then
	 local s = tostring(i)
	 local l = #s
	 length = length + l
	 if length >= start then
	    if not digits then
	       digits = s:sub(start - length + l, l + 1)
	    else
	       digits = digits .. s
	    end
	    if #digits >= 5 then
	       return digits:sub(1, 5)
	    end
	 end
      end
      i = i + 2
   end
end

local function main()
   print("minion[" .. arg[1] .. "]=" .. answer(arg[1]))
end

main()

