#!/usr/bin/python

import sys
import math

primes = [ 0 ] * 1000000
primes[0] = 2
pi = 0

def isPrime(num):
    global primes
    global pi
    limit = int(math.sqrt(num))
    for i in primes:
        if i == 0 or i > limit: break
        if num % i == 0: return False
    pi += 1
    primes[pi] = num
    return True

# Returns 5 digits from concatenated prime numbers, starting at provided offset.
# Instead of concatenating all the primes, we keep track of the length and only
# start collecting once we reach the right offset.  We also hard-code the result
# for `0` so that we can always increment candidates by 2.
def answer(n):
    start = int(n)
    if start == 0:
        return "23571"
    digits = None
    length = 0
    i = 3
    while True:
        if isPrime(i):
            s = str(i)
            l = len(s)
            length += l
            if length >= start:
                if digits is None:
                    digits = s[start - length + l - 1:l]
                else:
                    digits += s
                if len(digits) >= 5:
                    return digits[0:5]
        i += 2

def main():
    arg = sys.argv[1]
    print("minion[" + arg + "]=" + answer(arg))

if __name__ == "__main__":
    main()
