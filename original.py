import sys
import math

def isfoo(num):
    if (num in (1, 2, 3, 5)): return 1
    if (num // 2 * 2 == num ): return 0
    for i in range(2, int(math.sqrt(num))+1):
        if (num // i * i == num): return 0
    return 1

def answer(n):
    LEN = 5
    minion = "2";
    i = 3;
    nn = int(n)
    while ( 1 ):
        if ( isfoo(i) == 1):
            minion += str(i)
            if (len(minion)>nn+LEN):
                print(minion)
                return minion[nn:nn+LEN]
                break
        i+=1

def main():
    print("minion["+sys.argv[1]+"]="+answer(sys.argv[1]))

if __name__ == "__main__":
    main()
