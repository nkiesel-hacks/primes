val primes = IntArray(1_000_000)
var pi = 0

fun isPrime(num: Int): Boolean {
    val limit = Math.sqrt(num.toDouble()).toInt()
    for (i in primes) {
        if (i == 0 || i > limit) break
        if (num.rem(i) == 0) return false
    }
    primes[++pi] = num
    return true
}

fun answer(n: String): String {
    val start = n.toInt()
    if (start == 0) return "23571"
    var digits: String? = null
    var length = 0
    var i = 3
    while (true) {
        if (isPrime(i)) {
            val s = i.toString()
            val l = s.length
            length += l
            if (length >= start) {
                if (digits == null)
                    digits = s.substring(start - length + l - 1, l)
                else
                    digits += s
                if (digits.length >= 5)
                    return digits.substring(0, 5)
            }
        }
        i += 2
    }
}

fun main(args: Array<String>) {
    val arg = args[0]
    primes[0] = 2
    println("minion[${arg}]=${answer(arg)}")
}

