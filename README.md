# Weird Prime Number Challenge #

The challenge does mandate Python but I read this too late, so I ported the
Python code to Kotlin and Lua after improving it.

The primary contender for the challange is `optimized.py`.

In addition, there is a "cheating" contender `stateful.py` which remembers
computed primes across invocations.  This will obviously speed up the code
tremendously on repeated calls, but the code is slower if it does not have
access to precomputed primes.

On my laptop (i7-4700MQ; 32GB RAM; Debian unstable) I use the following tools:

| Interpreter | Version     |
| ----------- | ----------- |
| python2     | 2.7.15      |
| python3     | 3.7.15      |
| pypy        | 6.0.0       |
| lua         | 5.2.4       |
| luajit      | 2.1.0-beta3 |
| kotlin      | 1.2.51      |
| java        | 1.8.0_171   |

With these I get the following results (all for value 5555555):

| Language      | Interpreter | Time (seconds) | Comment               |
| ------------- | ----------- | -------------: | -----------           |
| original.py   | python2     | 239.790        |                       |
| original.py   | python3     | 195.100        | added `()` to `print` |
| original.py   | pypy        | 377.870        |                       |
| optimized.py  | python2     | 27.605         |                       |
| optimized.py  | python3     | 29.989         |                       |
| optimized.py  | pypy        | 4.871          |                       |
| optimized.lua | lua         | 23.978         |                       |
| optimized.lua | luajit      | 1.983          |                       |
| optimized.kt  | java        | 1.308          |                       |
| stateful.py   | python2     | 61.300         | first call            |
| stateful.py   | python2     | 0.224          | second call           |
| stateful.py   | python3     | 27.156         | first call            |
| stateful.py   | python3     | 0.266          | second call           |
| stateful.py   | pypy        | 4.689          | first call            |
| stateful.py   | pypy        | 0.102          | second call           |
|               |             |                |                       |

To compile the Kotlin version, use `kotlinc optimized.kt`

To run the Kotlin version, use `kotlin OptimizedKt 5555555` (with Kotlin 1.2.51
or higher) or `java -cp kotlin-stdlib.jar:. OptimizedKt 5555555` (with Java 8 or higher).

I'm sure that these times can be beaten, but it's interesting to see that Lua as
a high-level scripting language (when using the just-in-time compiler) gets
close to the Kotlin time and handily beats Python.

In addition, Python3 speed is normally in the same ballpark as Python2 speed for
this code, with the exception of `stateful.py`.  My suspicion is that the
generator approach used in `stateful.py` is much more optimized in Python3 than
it is in Python2.

Another anomaly is that `original.py` is _much_ slower with `pypy` than with
either python2 or python3.  I have no idea why that happens.
