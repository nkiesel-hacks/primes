#!/usr/bin/python

import sys
import pickle
from math import sqrt
from array import array

class Primes:
    filename = "primes.data"

    def __init__(self):
        self.primes = array("I")
        self.load()

    def load(self):
        try:
            with open(Primes.filename, "rb") as f:
                self.primes.fromfile(f, pickle.load(f))
                self.need_save = False
        except IOError:
            self.primes.fromlist([3, 5, 7])
            self.need_save = True

    def save(self):
        if self.need_save:
            with open(Primes.filename, "wb") as f:
                pickle.dump(self.primes.buffer_info()[1], f)
                self.primes.tofile(f)

    def notPrime(self, num):
        limit = int(sqrt(num))
        for p in self.primes:
            if num % p == 0: return True
            if p > limit: return False

    def next(self):
        for p in self.primes:
            yield p
        
        while True:
            prime = self.primes[-1] + 2
            while self.notPrime(prime):
                prime += 2
            self.primes.append(prime)
            self.need_save = True
            yield prime


# Returns 5 digits from concatenated prime numbers, starting at provided offset.
# Instead of concatenating all the primes, we keep track of the length and only
# start collecting once we reach the right offset.  We also hard-code the result
# for `0` so that we can always increment candidates by 2.
def answer(n):
    start = int(n)
    if start == 0:
        return "23571"
    digits = None
    length = 0
    primes = Primes()

    for p in primes.next():
        s = str(p)
        l = len(s)
        length += l
        if length >= start:
            if digits is None:
                digits = s[start - length + l - 1:l]
            else:
                digits += s
            if len(digits) >= 5:
                primes.save()
                return digits[0:5]


def main():
    arg = sys.argv[1]
    print("minion[" + arg + "]=" + answer(arg))

if __name__ == "__main__":
    main()
